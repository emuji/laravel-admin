<?php

namespace Emuji\Admin\Layout;

interface Buildable
{
    public function build();
}
