<?php

namespace Emuji\Admin\Grid\Filter;

class Ilike extends Like
{
    protected $operator = 'ilike';
}
