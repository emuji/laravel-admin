<?php

namespace Emuji\Admin\Grid\Filter;

class StartsWith extends Like
{
    protected $exprFormat = '{value}%';
}
