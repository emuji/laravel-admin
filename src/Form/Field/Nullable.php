<?php

namespace Emuji\Admin\Form\Field;

use Emuji\Admin\Form\Field;

class Nullable extends Field
{
    public function __construct()
    {
    }

    public function __call($method, $parameters)
    {
        return $this;
    }
}
