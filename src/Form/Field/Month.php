<?php

namespace Emuji\Admin\Form\Field;

class Month extends Date
{
    protected $format = 'MM';
}
